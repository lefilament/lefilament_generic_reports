{
    'name': 'Template Générique',
    'summary': 'Création d un Template générique de rapport',
    'description': 'Création d un Template générique de rapport',
    'author': 'LE FILAMENT',
    'version': '12.0.1.1.0',
    'license': "AGPL-3",
    'depends': ['base','web', 'sale'],
    'qweb': [],
    'data': [
        'views/res_config_settings_views.xml',
        'views/report_templates.xml',
        'views/res_company_views.xml',
        'views/report_sale.xml',
        'data/report_layout.xml',
    ],
}
