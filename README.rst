.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


====================
Template Générique
====================

Ajout d'un modèle de Template

Hérite et modifie le module sale Odoo

* Modification du layout
* Ajout d'un rapport générique
* Ajout d'un format de papier
* Ajout des numéros d'OF et du type de société dans `res_company`
* Ajout des champs CSS pour configurer le style des rapports

Configuration
=============

Pré-requis
----------

* Configurer les informations de la société : `Configuration >> Sociétés` 
* Configurer le Format de Papier par défaut dans `Configuration >> Paramètres généraux` sur "Facture/Devis Le Filament"

Configuration des rapports
--------------------------

Il est possible d'affecter des couleurs différentes aux devis et aux factures. Les champs textes sont à remplir comme des CSS. 
Les typos disponibles sont : Lato, DejaVu Sans, DejaVu Serif, Roboto, Oswald, Helvetica

* Devis : `Ventes >> Configuration` puis configurer les différents champs de la section "Configuration des Rapports"
* Factures : `Comptabilité >> Configuration` puis configurer les différents champs de la section "Configuration des Rapports"

.. image:: /lefilament_generic_reports/static/img/template.png
   :alt: Template par defaut
   :width: 400px


Modification du Header
----------------------

* `Configuration >> Paramètres généraux >> Editer l'entête de la page externe` 


Modification du Footer
----------------------

* `Configuration >> Paramètres généraux >> Editer le pied de page de la page externe` 


Credits
=======

Contributors
------------

* Benjamin Rivier <benjamin@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by `Le Filament <https://le-filament.com>`_