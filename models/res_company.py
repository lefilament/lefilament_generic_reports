# Copyright 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class FilResCompany(models.Model):
    _inherit = 'res.company'

    formation = fields.Char('N° Organisme de formation')
    company_type = fields.Char('Type de société')
    body_font = fields.Text('Body Font', default="font-family: 'DejaVu Sans';")
    tab_font = fields.Text('Tab Font', default="font-family: 'Helvetica';")
    footer_font = fields.Text('Footer Font', default="font-family: 'Lato';")